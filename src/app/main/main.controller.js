(function() {
  'use strict';

  angular
    .module('beelineDocus')
    .controller('MainController', MainCtrl);

  /** @ngInject */
    function MainCtrl(require, $state, dataTransfer) {
        var vm = this;

        vm.submit = function() {
            vm.formMessage = {};
            var testEmail = require.email(vm.userEmailMod);
            if(testEmail == 'empty') {
                vm.formMessage['status'] = 'error';
                vm.formMessage['message'] = 'Поле email обязательно для заполнения';
            } else if(testEmail == 'invalid') {
                vm.formMessage['status'] = 'error';
                vm.formMessage['message'] = 'Вы не корректно ввели email';
            } else if(testEmail == 'valid') {
                vm.formMessage['status'] = 'success';
                dataTransfer.setData('email', vm.userEmailMod);
                /*var apiService = apiService.query('email', vm.userEmailMod);
                apiService.then(function(response) {
                    if (response.status == 200) {
                        $state.go('upload');
                    }
                });*/
                $state.go('upload');
            }
        }

    }
})();
