(function() {
  'use strict';

  angular
    .module('beelineDocus', ['ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'ngFileUpload']);

})();
