(function() {
    'use strict';

    angular
        .module('beelineDocus')
        .controller('ResultController', ResultCtrl);

    /** @ngInject */
    function ResultCtrl(apiService, $state) {
        var vm = this;
        vm.finish = false;

        jobStatus();

        function jobStatus() {
            var status = apiService.query('status');
            status.then(function(response) {
                if (response.data.jobStatus == 'SUCCESS') {
                    getResult();
                } else if(response.data.jobStatus == 'RUNNING') {
                    jobStatus();
                } else {
                    $state.go('upload');
                }
            });
        }

        function getResult() {
            var analise = apiService.query('analysis');
            analise.then(function (response) {
                if (response.status == 200) {
                    vm.finish = true;
                    vm.images = response.data.images;
                    for(var f = 0, fcount = vm.images.length; f < fcount; f++) {
                        switch (vm.images[f].answer) {
                            case 'GREEN':
                                vm.images[f].massage = 'Файл является оригиналом либо близким к этому файлом!';
                                vm.images[f].massageMore = 'Потенциально опасных нарушений не выявленно.';
                                break;
                            case 'YELLOW':
                                vm.images[f].massage = 'Требуется дополнительная проверка!';
                                vm.images[f].massageMore = 'Ответ придет на указанную вами почту в течении 15 минут. <br /> Спасибо за ожидание!';
                                break;
                            case 'RED':
                                vm.images[f].massage = 'Файл не является оригиналом!';
                                vm.images[f].massageMore = vm.images[f].details;
                                break;
                        }
                        vm.images[f].name = vm.images[f].imagePath.substr((vm.images[f].imagePath.lastIndexOf('/') + 1), vm.images[f].imagePath.length);
                    }
                }
            });
        }

    }
})();
