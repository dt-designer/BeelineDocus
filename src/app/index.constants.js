/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('beelineDocus')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
