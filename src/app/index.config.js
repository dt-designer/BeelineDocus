(function() {
  'use strict';

  angular
    .module('beelineDocus')
    .service('apiConfig', apiConfig)
    .config(config);

    function apiConfig() {
        this.conf = {
            server   : 'http://dev2.smtdp.de:8080/',
            key      : 'XUCMXPBMGMGYKQ',
            lang     : 'ru',
            service  : 'pmi'
        };
        return this.conf;
    }

    function config($logProvider) {

        // Enable log
        $logProvider.debugEnabled(true);

    }

})();
