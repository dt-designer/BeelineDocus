(function() {
  'use strict';

  angular
    .module('beelineDocus')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');

  }

})();
