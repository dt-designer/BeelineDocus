(function() {
    'use strict';

    angular
        .module('beelineDocus')
        .service('require', require);

    /** @ngInject */
    function require() {
        var vm = this;

        vm.email = function(data) {
            var regex = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i,
                status;
            if(!data) {
                status = 'empty';
            } else if(!regex.test(data)) {
                status = 'invalid';
            } else {
                status = 'valid';
            }
            return status;
        };

        vm.file = function(fileList) {
            return new Promise( function(resolve, reject) {
                var errorFiles = [], successFiles = [];
                for (var i = 0, icount = fileList.length; i < icount; i++) {
                    if(fileList[i].type != 'image/jpeg' || fileList[i].type == 'image/tiff' || fileList[i].type == 'application/pdf') {
                        fileList[i].error = {
                            message: 'Неподдерживаемый тип файла, файл не будет загружен.',
                            code: '-11'
                        };
                        errorFiles.push(fileList[i]);
                    } else {
                        successFiles.push(fileList[i]);
                    }
                }
                var newFileList = {
                    'success': successFiles,
                    'error': errorFiles
                };
                resolve(newFileList);
            })
        };
    }

})();
