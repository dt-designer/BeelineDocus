(function() {
    'use strict';

    angular
        .module('beelineDocus')
        .service('dataTransfer', dataTransfer);

    /** @ngInject */
    function dataTransfer() {
        var vm = this,
            dataStorage = [];

        vm.setData = function(name, data) {
            dataStorage[name] = data;
        };

        vm.getData = function(name) {
            return dataStorage[name];
        };

    }
})();
