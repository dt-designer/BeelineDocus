(function() {
    'use strict';

    angular
        .module('beelineDocus')
        .service('apiService', api);

    /** @ngInject */
    function api(apiConfig, $http, dataTransfer) {
        var vm = this;

        vm.query = function(method) {
            var url, headers;

            switch (method) {
                case 'analysis':
                    url = apiConfig.server + 'resources/jobs/' + dataTransfer.getData('jobId') + '/result?key=' + apiConfig.key + '&lang=' + apiConfig.lang;
                    headers = {
                        'X-Client-Id': apiConfig.service,
                        'Accept': 'application/json'
                    };
                    break;
                case 'status':
                    url = apiConfig.server + 'resources/jobs/' + dataTransfer.getData('jobId') + '?key=' + apiConfig.key + '&lang=' + apiConfig.lang;
                    headers = {
                        'X-Client-Id': apiConfig.service,
                        'Accept': 'application/json, text/plain, */*'
                    };
                    break;
            }

            // Отправка данных на сервер
            return $http({
                method: 'GET',
                url: url,
                headers: headers
            }).then(SuccessFn, ErrorFn);

            // Если ответ без ошибок
            function SuccessFn(response) {
                return response;
            }

            // Работаем с ошибками
            function ErrorFn(response) {
                if(response.status == '404') {
                    alert('Ваш запрос пустой, попробуйте еще раз!');
                } else {
                    alert('Произошла ошибка подключения к серверу, попробуйте еще разок!');
                }
                return response;
            }
        }

    }
})();
