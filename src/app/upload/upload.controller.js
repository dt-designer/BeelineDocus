(function() {
  'use strict';

  angular
    .module('beelineDocus')
    .controller('UploadController', UploadCtrl);

  /** @ngInject */
  function UploadCtrl($scope, Upload, require, $state, apiConfig, dataTransfer) {
      var vm = this;

      if(!dataTransfer.getData('email')) $state.go('main');

      var fields = '{"analysis": {"outputTypes": ["PDF"],"paintings": [],"influenceAlgorithmsInReport": [],"detailedResultsFor": ["GREEN","YELLOW","RED"],"groupDetailedResultsFor": ["GREEN","YELLOW","RED"],"langCode": "ru","extractImageInfo": false,"serviceId": "TRUST","imageInspectionStrategyType": "RELAXED","minimumImageSizeMp": 0,"geoConfig": {"enabled": false,"coordinates": null,"maxRangeInMeters": 100,"forceGeolocation": false},"whiteListSoftware": [],"blackListSoftware": [],"groupConfig": {"enabled": false}}}';

      vm.submit = function() {
          $state.go('result');
      };

      $scope.$watch('files', function () {
          if($scope.files) {
              vm.upload($scope.files);
          }
      });
      vm.upload = function (files) {
          vm.fileList = [];

          var requireFiles = require.file(files);
          requireFiles.then(function (fileList) {
              Upload.upload({
                  url: apiConfig.server + 'resources/jobs?timezoneOffset=3&key=' + apiConfig.key + '&lang=' + apiConfig.lang,
                  method: 'POST',
                  headers: {
                      'X-Client-Id': apiConfig.service
                  },
                  file: fileList.success,
                  fields: { jobSettings: fields }
              }).then(function (resp) {
                  dataTransfer.setData('jobId', resp.data.jobId);
              }, function (resp) {
                  vm.error = resp.status;
              }, function (evt) {
                  fileList.success = evt.config.file;
                  for (var i = 0, icount = fileList.success.length; i < icount; i++) {
                      fileList.success[i].progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                  }
              });
              vm.fileList = vm.fileList.concat(fileList.error, fileList.success);
              for (var f = 0, fcount = vm.fileList.length; f < fcount; f++) {
                  vm.fileList[f].fileSize = fileSize(vm.fileList[f].size);
                  vm.fileList[f].elmId = vm.fileList[f].$$hashKey;
              }
          });
      };

      vm.abort = function(id) {
          for(var i = 0, icount = vm.fileList.length; i < icount; i++) {
              if(vm.fileList[i].elmId == id) {
                  vm.fileList.splice(i, 1);
              }
          }
          //Upload.abort();
      };

      function fileSize(data) {
          data = (data / 1024);
          if (data < 1024) {
              data = Math.round(data) + ' KB';
          } else {
              data = (data / 1024).toFixed(2) + ' MB';
          }
          return data;
      }

  }
})();
