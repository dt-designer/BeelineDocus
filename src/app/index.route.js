(function() {
  'use strict';

  angular
    .module('beelineDocus')
    .config(routerConfig);

  /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .state('upload', {
                url: '/upload',
                templateUrl: 'app/upload/upload.html',
                controller: 'UploadController',
                controllerAs: 'upload'
            })
            .state('result', {
                url: '/result',
                templateUrl: 'app/result/result.html',
                controller: 'ResultController',
                controllerAs: 'result'
            });

        $urlRouterProvider.otherwise('/');
    }

})();
