<h1>Тестовая работа "BeelineDocus"</h1>
<h3>Инструкция по установки</h3>

<p>Первое, устанавливаем NodeJS (Если не установлен)</p>
<p>Далее устанавливаем  компоненты <code>yo</code>, <code>gulp</code> и <code>bower</code>:</p>
<pre>npm install -g yo gulp bower</pre>
<p>Создаем дерикторию проекта и переходим в нее из командной строки</p>
<p>&nbsp;</p>
<p>Скачиваем проект</p>
<pre>git init</pre>
<pre>git pull https://gitlab.com/dt-designer/BeelineDocus.git</pre>
<p>&nbsp;</p>
<p>Устанавливаем компоненты для среды разработки</p>
<pre>npm install</pre>
<p>Последний шаг, устанавливаем компоненты проекта</p>
<pre>bower install</pre>
<p><i>Теперь все установленно и готово к работе</i></p>
<p>&nbsp;</p>
<h3>Компиляция проекта</h3>
<ul>
    <li>Для запуска и тестирования проекта <code>gulp serve</code> (создается веб сервер и открывается браузер с адресом http://localhost:3000/ так же активна функция liveReload)</li>
    <li>Для компиляции проекта в продакшн <code>gulp build</code></li>
</ul>